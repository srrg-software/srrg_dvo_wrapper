# SRRG_DVO_WRAPPER
A wrapper package for [dvo (bart-forked-v)](https://github.com/bartville/dvo_slam) to load and process `txtio/boss` dataset, and visualize the resulting reconstruction with `nicp_gui`

## Dependencies
* [srrg_core](https://gitlab.com/srrg-software/srrg_core)
* [bartville/sophus](https://github.com/bartville/Sophus) - _Courtesy of H.Strasdat_
* [bartville/dvo](https://github.com/bartville/dvo_slam) - _Courtesy of C.Kerl, J.Sturm, D.Cremers (TUM)_

## Usage

The app provides a help by typing `-h`. The standard usage is:

    rosrun srrg_dvo srrg_dvo_app -t <depth_topic> -rgbt <rgb_topic> -o <dvo_output.txt> -eval <dvo_output_eval.txt> -omega-intensity <float> dump.txt

it will generate a `dvo_output.txt` file containing the odom field filled with dvo output, and an evalution file 'dvo_output_eval.txt' in TUM format.
This file can be visualized with `srrg_nicp_tracker_gui` as

    rosrun srrg_nicp_tracker_gui srrg_nicp_tracker_gui_app -config do_nothing -bpr 10 -t <depth_topic> -rgbt <rgb_topic> dvo_output.txt
    
**do not forget** the `do_nothing` configuration, otherwise 'nicp' will optimize using the `dvo_output` as initial guess. `-bpr 10` (_bad point ratio_) is used to avoid the tracker to break the track due to conflicting points.

In order to perform an evaluation of the computed odometry ('dvo_output_eval.txt'), TUM guys provide an online evaluation tool [here](http://vision.in.tum.de/data/datasets/rgbd-dataset/online_evaluation) for the TUM benchmark suite.

For debugging purposes, the intensity channel contribution can be suppressed by typing `-omega-intensity 0`
