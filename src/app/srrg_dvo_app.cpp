#include <iostream>
#include <vector>
#include <fstream>
#include <limits>
#include <iomanip>

#include <Eigen/Core>

#include <dvo/core/surface_pyramid.h>
#include <dvo/dense_tracking.h>

#include <srrg_system_utils/system_utils.h>
#include <srrg_messages/message_reader.h>
#include <srrg_messages/message_seq_synchronizer.h>
#include <srrg_messages/message_timestamp_synchronizer.h>
#include <srrg_messages/message_writer.h>
#include <srrg_messages/pinhole_image_message.h>
#include <srrg_messages/spherical_image_message.h>
#include <srrg_messages/static_transform_tree.h>

using namespace std;
using namespace Eigen;
using namespace srrg_core;
using namespace dvo;
using namespace dvo::core;

typedef Eigen::Matrix<double, 7, 1> Vector7d;

DenseTracker *dvo_tracker = 0;
RgbdCameraPyramid *camera = 0;
RgbdImagePyramidPtr current, reference;
float omega_intensity;

void dvoInitCose(const DenseTracker::Config &dvo_tracker_cfg,
                 const IntrinsicMatrix &intrinsics, const float &width,
                 const float &height) {
  dvo_tracker = new DenseTracker(dvo_tracker_cfg);
  dvo_tracker->setOmegaIntensity(omega_intensity);
  camera = new RgbdCameraPyramid(width, height, intrinsics);
  camera->build(dvo_tracker_cfg.getNumLevels());
  cerr << "Tracker Initialized " << endl;
}

Vector7d t2vFull(const Eigen::Isometry3d& iso){
  Vector7d v;
  v.head<3>() = iso.translation();
  Eigen::Quaterniond q(iso.linear());
  v(3) = q.x();
  v(4) = q.y();
  v(5) = q.z();
  v(6) = q.w();
  return v;
}


const char *banner[] = {
    "\n\nUsage:  srrg_dvo_app -t <depth_topic> -rgbt <rgbt_topic> -o "
    "<output_file.txt> [Options] <dump_file.txt> ",
    "Example:  srrg_dvo_app -t /camera/depth/image_raw -rgbt "
    "/camera/rgb/image_raw -o dvo_output.txt dataset.txt\n\n",
    "Options:\n", "------------------------------------------\n",
    "-t <string>                 DEPTH image topic name, as in the txtio file",
    "-rgbt <string>              RGB image topic name, as in the txtio file",
    "-o <string>                 output dvo file in txtio format",
    "-eval <string>              generate an evaluation file in TUM format",
    "-omega-intensity <float>    multiplier of the intensity error/omega",
    "-h                          this help\n", 0};

int main(int argc, char **argv) {
  string depth_topic;
  string rgb_topic;

  std::vector<std::string> depth_topics;
  std::vector<std::string> rgb_topics;
  string filename;
  string output_filename = "dvo_output.txt";
  int c = 1;
  string evaluation_filename = "dvo_output_eval.txt";

  omega_intensity = 1.f;
  
  while (c < argc) {
    if (!strcmp(argv[c], "-h")) {
      printBanner(banner);
      return 0;
    } else if (!strcmp(argv[c], "-t")) {
      c++;
      depth_topic = argv[c];
      depth_topics.push_back(depth_topic);
    } else if (!strcmp(argv[c], "-rgbt")) {
      c++;
      rgb_topic = argv[c];
      rgb_topics.push_back(rgb_topic);
    } else if (!strcmp(argv[c], "-o")) {
      c++;
      output_filename = argv[c];
    } else if (!strcmp(argv[c], "-eval")) {
      c++;
      evaluation_filename = argv[c];
    } else if (!strcmp(argv[c], "-omega-intensity")) {
      c++;
      omega_intensity = std::atof(argv[c]);
    } else {
      filename = argv[c];
      break;
    }
    c++;
  }
  
  cerr << "depth_topic:         " << depth_topic << endl;
  cerr << "rgb_topic:           " << rgb_topic << endl;
  cerr << "filename:            " << filename << endl;
  cerr << "out filename:        " << output_filename << endl;
  cerr << "evaluation filename: " << evaluation_filename << endl;
  cerr << "omega-intensity:     " << omega_intensity << endl;

  // bdc, file reading
  MessageReader reader;
  reader.open(filename);

  MessageWriter writer;
  writer.open(output_filename);

  std::ofstream evaluation_file;
  evaluation_file.open(evaluation_filename);
  
  bool first_msg = true;
  IntrinsicMatrix intrinsics;
  DenseTracker::Config dvo_tracker_cfg;
  Eigen::Affine3d accumulated_transform(Eigen::Affine3d::Identity());
  Eigen::Affine3d latest_absolute_transform_;
  latest_absolute_transform_.setIdentity();
  Eigen::Affine3d from_baselink_to_asus;
  from_baselink_to_asus.setIdentity();

  //timing stats
  double start_time = srrg_core::getTime();
  int number_of_frames_current_window = 0;
  const double measurement_interval_seconds = 1.0;
  
  /*  while (reader.good()) {

    BaseMessage* msg = reader.readMessage();
    if (!msg) {
      continue;
    }
    BaseImageMessage *base_img = dynamic_cast<BaseImageMessage *>(msg);
    if (!base_img) {
      delete msg;
      continue;
    }

    PinholeImageMessage *base_depth_img = 0, *base_rgb_img = 0;
    synchronizer.putMessage(base_img);
    if (synchronizer.messagesReady()) {
      base_depth_img =
          dynamic_cast<PinholeImageMessage *>(synchronizer.messages()[0].get());
      if (synchronizer.messages().size() > 1) {
        base_rgb_img = dynamic_cast<PinholeImageMessage *>(
            synchronizer.messages()[1].get());
      }
    }

    if (!base_rgb_img) {
      continue;
    }
  */

  std::vector<MessageTimestampSynchronizer> synchronizers(depth_topics.size());
  for (size_t i = 0; i < depth_topics.size(); i++) {
    std::vector<string> depth_plus_rgb_topic;
    depth_plus_rgb_topic.push_back(depth_topics[i]);
    depth_plus_rgb_topic.push_back(rgb_topics[i]);
    synchronizers[i].setTopics(depth_plus_rgb_topic);
    synchronizers[i].setTimeInterval(0.03);
  }

  double dvo_cumulative_time = 0.f;
  
  while (reader.good()) {
        BaseMessage* msg = reader.readMessage();
    if (!msg) { continue; }
    BaseImageMessage* base_img = dynamic_cast<BaseImageMessage*>(msg);
    if (!base_img) {
      delete msg;
      continue;
    }
    synchronizers[0].putMessage(base_img);
    if (! synchronizers[0].messagesReady()) {
      continue;
    }

    PinholeImageMessage *depth_msg = 0, *rgb_msg = 0;
    depth_msg =
      dynamic_cast<PinholeImageMessage*>(synchronizers[0].messages()[0].get());
    rgb_msg =
      dynamic_cast<PinholeImageMessage*>(synchronizers[0].messages()[1].get());

    if (!depth_msg || ! rgb_msg) {
      continue;
    }
  
    if (first_msg) {
      // init tracker and continue
      const Eigen::Matrix3f &camera_matrix = rgb_msg->cameraMatrix();
      float fx = camera_matrix(0, 0);
      float fy = camera_matrix(1, 1);
      float cx = camera_matrix(0, 2);
      float cy = camera_matrix(1, 2);
      intrinsics = IntrinsicMatrix::create(fx, fy, cx, cy);
      dvo_tracker_cfg = DenseTracker::getDefaultConfig();
      cv::Mat rgb_image = rgb_msg->image();
      int width = rgb_image.cols;
      int height = rgb_image.rows;
      dvoInitCose(dvo_tracker_cfg, intrinsics, width, height);
      first_msg = false;
      continue;
    }

    // here we can get the rgb and depth images
    cv::Mat rgb_in = rgb_msg->image();
    cv::Mat depth_in = depth_msg->image();

    cv::Mat intensity, depth;

    if (rgb_in.channels() == 3) {
      cv::Mat tmp;
      cv::cvtColor(rgb_in, tmp, CV_BGR2GRAY, 1);

      tmp.convertTo(intensity, CV_32F);
    } else if (rgb_in.channels() == 4) {
      cv::Mat tmp;
      cv::cvtColor(rgb_in, tmp, CV_BGRA2GRAY, 1);
      tmp.convertTo(intensity, CV_32F);
    } else {
      rgb_in.convertTo(intensity, CV_32F);
    }

    if (depth_in.type() == CV_16UC1) {
      SurfacePyramid::convertRawDepthImageSse(depth_in, depth, 0.001);
    } else {
      depth = depth_in;
    }

    reference.swap(current);
    current = camera->create(intensity, depth);

    Eigen::Affine3d first;

    if (!reference) {
      accumulated_transform =
          latest_absolute_transform_ * from_baselink_to_asus;
      first = accumulated_transform;
      continue;
    }

    Eigen::Affine3d transform;

    double dvo_matching_time = srrg_core::getTime();
    bool success = dvo_tracker->match(*reference, *current, transform);
    dvo_matching_time = srrg_core::getTime() - dvo_matching_time;
    dvo_cumulative_time += dvo_matching_time;
    
    if (success) {
      //            cerr << "s";
      accumulated_transform = accumulated_transform * transform;
    } else {
      cerr << "f " << endl;
      reference.swap(current);
      cerr << "fail\n";
    }
    Eigen::Isometry3d iso;
    iso.linear() = accumulated_transform.linear();
    iso.translation() = accumulated_transform.translation();
    depth_msg->setOdometry(iso.cast<float>());
    rgb_msg->setOdometry(iso.cast<float>());
    writer.writeMessage(*depth_msg);
    writer.writeMessage(*rgb_msg);

    evaluation_file << std::setprecision(std::numeric_limits<double>::digits10)
		    << rgb_msg->timestamp() << " "
		    << t2vFull(iso).transpose() << std::endl;

    ++number_of_frames_current_window;
    const double total_duration_seconds_current = srrg_core::getTime()-start_time;
    if(total_duration_seconds_current > measurement_interval_seconds) {
      printf("current fps: %5.2f\n", (double)number_of_frames_current_window/total_duration_seconds_current);
      number_of_frames_current_window = 0;
      start_time = srrg_core::getTime();
    }

    
    cv::imshow("rgb", rgb_in);
    cv::imshow("depth", depth_in * 5);
    cv::waitKey(1);

    synchronizers[0].reset();
  }

  cerr << "\ndvo cumulative time:\t" << dvo_cumulative_time << endl;
  
  cerr << "\nIf you have 'srrg_nicp_tracker_gui' installed, you can visualize the output of the dvo reconstruction typing:\n"
       << "\n\t rosrun srrg_nicp_tracker_gui srrg_nicp_tracker_gui_app -config do_nothing -bpr 10 -t "
       << depth_topic
       << " -rgbt "
       << rgb_topic
       << " "
       << output_filename << endl << endl; 

  evaluation_file.close();

  
  return 0;
}
